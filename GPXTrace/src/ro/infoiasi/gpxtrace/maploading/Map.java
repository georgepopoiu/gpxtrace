package ro.infoiasi.gpxtrace.maploading;

import java.util.Collection;
import java.util.HashMap;

import ro.infoiasi.gpxtrace.maploading.exceptions.DrawException;
import ro.infoiasi.gpxtrace.maploading.exceptions.InstallMapException;
import ro.infoiasi.gpxtrace.maploading.exceptions.LoadMapException;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.Color;
import android.widget.FrameLayout;

/**
 * Class that can be used to manage a map independently of its type.
 * @author george
 */
public abstract class Map {		
	
	/**
	 * Static method responsible for installing a map.
	 * 
	 * !!! IMPORTANT !!! 
	 * It should be called in the onCreate() method of the activity on which we want to display the map. 
	 * 
	 * @param mapType The type of the map you want to use.
	 * @param mapName A unique string that identifies the map.
	 * @param activity The parent activity of the map.
	 * @param containerId The id of the containing UI element.
	 * @throws InstallMapException Call getMessage() on the returned exception to see the reason why the installation failed.
	 */
	public static void installMap(MapType mapType, String mapName, Activity activity, int containerId)
																				throws InstallMapException {
		if(!handlers.containsKey(mapType)) {
			throw new InstallMapException("This map type is not supported yet.");		
		}
		
		handlers.get(mapType).handleInstall(mapName, activity, containerId);
	}
	
	
	/**
	 * Static method responsible for loading a map.
	 * 
	 * !!IMPORTANT!!
	 * It should be called in the onResume method of the activity that displays the map.
	 * 
	 * @param mapType The type of the map we want to load.
	 * @param mapName The name that uniquely identifies our map.
	 * @return A Map object that can be used to manipulate the map.
	 * @throws LoadMapException Call getMessage() on the returned exception to see why the loading failed.
	 */
	public static Map loadMap(MapType mapType, String mapName) throws LoadMapException {
		if(!handlers.containsKey(mapType)) {
			throw new LoadMapException("This map type is not supported yet.");	
		}
		
		return handlers.get(mapType).handleLoad(mapName);
	}
	
	
	/**
	 * Draws a polyline on the map using the given locations.
	 * 
	 * @param locations The locations to be used in order to draw the polyline.
	 * @throws DrawException Use e.getMessage() to see the reason dor the exception.
	 */
	public abstract void drawPolyline(Collection<Location> locations) throws DrawException;
	
	
	/**
	 * Draws a polygon on the map using the given location
	 * @param locations The locations that will be used to draw the polygon
	 * @throws DrawException In case it appears an exception we handle it in DrawException class with the method e.getMessage
	 */
	public abstract void drawPolygon(Collection<Location> locations) throws DrawException;
	
	
	/**
	 * Method responsible for drawing points on map
	 * 
	 * @param myPoint Coordinates for drawing a marker
	 */
	public abstract void drawPoint(Location myPoint);
	
	
	/**
	 * Method responsible for zooming the map to a specific location
	 * 
	 * @param myPoint Coordinates for center
	 */
	public abstract void zoom(Location myPoint);
	
		
	private int lineWidth = 10;	
		
	/**
	 * Sets the width of the line that will be drawn on the map by the drawPolyline and drawPolygon methods.
	 * @param lineWidth
	 */
	public void setLineWidth(int lineWidth) {
		if(lineWidth <= 0)
			return;
		
		this.lineWidth = lineWidth;
	}
	
	/**
	 * Gets the width of the line that will be drawn on the map by the drawPolyline and drawPolygon methods.
	 * @return
	 */
	public int getLineWidth() {
		return lineWidth;
	}
	
	
	private int lineColor = Color.BLUE;
	
	/**
	 * Sets the color of the line that will be drawn on the map by the drawPolyline and drawPolygon methods.
	 * @param color
	 */
	public void setLineColor(int color) {
		this.lineColor  = color;
	}
	
	/**
	 * Gets the color of the line that will be drawn on the map by the drawPolyline and drawPolygon methods.
	 * @return
	 */
	public int getLineColor() {
		return lineColor;
	}
	
	
	float zoomLevel = 10;
	
	/**
	 * Gets the current zoom level of the map.
	 * @return
	 */
	public float getZoomLevel() {
		return zoomLevel;
	}

	/**
	 * Sets the current zoom level of the map.
	 * @param zoomLevel
	 */
	public void setZoomLevel(float zoomLevel) {
		if(zoomLevel < 0)
			return;
		
		this.zoomLevel = zoomLevel;
	}			
	
	
	/**
	 * Register of handlers for the supported map types.
	 */
	private static java.util.Map<MapType, MapHandler> handlers = new HashMap<MapType, Map.MapHandler>();
	static {
		handlers.put(MapType.GoogleMap, new GoogleMapHandler());
	}
		
	
	/**
	 * Interface for defining the behavior of a handler for a map type. 
	 * A handler should be able to treat the events of installing a map on an activity and loading a map.
	 * @author george
	 *
	 */
	public interface MapHandler {		
		public void handleInstall(String mapName, Activity activity, int containerId) throws InstallMapException;
		public Map handleLoad(String mapName) throws LoadMapException;
	}
	
	
}
