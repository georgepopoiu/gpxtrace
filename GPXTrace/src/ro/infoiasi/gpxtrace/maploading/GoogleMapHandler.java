package ro.infoiasi.gpxtrace.maploading;

import java.util.HashMap;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import android.app.Activity;
import android.app.FragmentManager;
import ro.infoiasi.gpxtrace.maploading.Map.MapHandler;
import ro.infoiasi.gpxtrace.maploading.exceptions.InstallMapException;
import ro.infoiasi.gpxtrace.maploading.exceptions.LoadMapException;

/**
 * Class used for installing and loading a Google Map.
 * @author george
 *
 */
public class GoogleMapHandler implements MapHandler {

	private java.util.Map<String, MapFragment> maps = new HashMap<String, MapFragment>();
	
	@Override
	public void handleInstall(String mapName, Activity activity, int containerId) 
																	throws InstallMapException {		
		if(maps.containsKey(mapName)) {
			throw new InstallMapException("A map with the same name was already installed.");
		}
		
		FragmentManager fragmentManager = activity.getFragmentManager();
		MapFragment mapFragment = MapFragment.newInstance();
		fragmentManager.beginTransaction().add(containerId, mapFragment, mapName).commit();
		
		maps.put(mapName, mapFragment);
	}

	@Override
	public Map handleLoad(String mapName) throws LoadMapException {
		if(!maps.containsKey(mapName)) {
			throw new LoadMapException("The map with the given name was not installed.");
		}
		
		GoogleMap googleMap = maps.get(mapName).getMap();
		
		return new GoogleMapAdapter(googleMap);		
	}
	

}
