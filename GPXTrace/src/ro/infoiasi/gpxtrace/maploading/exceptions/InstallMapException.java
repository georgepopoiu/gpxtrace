package ro.infoiasi.gpxtrace.maploading.exceptions;

public class InstallMapException extends Exception {

	public InstallMapException() {
		// TODO Auto-generated constructor stub
	}

	public InstallMapException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public InstallMapException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	public InstallMapException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

}
