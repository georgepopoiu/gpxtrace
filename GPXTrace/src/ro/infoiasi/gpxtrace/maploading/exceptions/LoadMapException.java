package ro.infoiasi.gpxtrace.maploading.exceptions;

public class LoadMapException extends Exception {

	public LoadMapException() {
		// TODO Auto-generated constructor stub
	}

	public LoadMapException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public LoadMapException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	public LoadMapException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

}
