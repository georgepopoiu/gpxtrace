package ro.infoiasi.gpxtrace.maploading;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ro.infoiasi.gpxtrace.maploading.exceptions.DrawException;
import android.graphics.Color;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class GoogleMapAdapter extends Map {

	private GoogleMap googleMap;
	
	public GoogleMapAdapter(GoogleMap googleMap) {
		this.googleMap = googleMap;			
	}
	
	
	/**
	 * @author Andrei
	 */
	@Override
	public void drawPolyline(Collection<Location> locations) throws DrawException {		
		try {
			List<LatLng> points = new ArrayList<LatLng>();
			
			for(Location location : locations) {
				points.add(new LatLng(location.getLatitude(), location.getLongitude()));
			}
			
			googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(points.get(0), getZoomLevel()));
	
			PolylineOptions options = new PolylineOptions().addAll(points)
					.width(getLineWidth()).color(getLineColor()).geodesic(true);
			
			googleMap.addPolyline(options);
		}
		catch(Exception e) {
			throw new DrawException(e);
		}
	}
	
	
	/**
	 * @author Iuliana
	 */
	@Override
	public void drawPolygon(Collection<Location> locations) throws DrawException {
		try {
			List<LatLng> points = new ArrayList<LatLng>();
				
			for(Location location : locations)  {
				points.add(new LatLng(location.getLatitude(), location.getLongitude()));
			}
				
			 googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(points.get(0), getZoomLevel()));
			 
			 PolygonOptions polygonOptions = new PolygonOptions().addAll(points).
					 strokeWidth(getLineWidth()).strokeColor(getLineColor());
			 
			 googleMap.addPolygon(polygonOptions);			
		}
		catch(Exception exception) {
			throw new DrawException(exception);			
		}		
	}
	
		
	@Override
	public void drawPoint(Location myPoint) {
		this.googleMap.addMarker(new MarkerOptions().position(
				new LatLng(myPoint.getLatitude(), myPoint.getLongitude()))
				.icon(BitmapDescriptorFactory.defaultMarker()));
	}

	@Override
	public void zoom(Location myPoint) {
		this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
				myPoint.getLatitude(), myPoint.getLongitude()), getZoomLevel()));
	}
	
}
