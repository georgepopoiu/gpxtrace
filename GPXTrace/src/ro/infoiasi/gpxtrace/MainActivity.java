package ro.infoiasi.gpxtrace;

import java.util.ArrayList;

import ro.infoiasi.gpxtrace.maploading.Location;
import ro.infoiasi.gpxtrace.maploading.Map;
import ro.infoiasi.gpxtrace.maploading.MapType;
import ro.infoiasi.gpxtrace.maploading.exceptions.DrawException;
import ro.infoiasi.gpxtrace.maploading.exceptions.InstallMapException;
import ro.infoiasi.gpxtrace.maploading.exceptions.LoadMapException;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends Activity {
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		try {
			//instalam harta cu numele mymap pe aceasta activitate in R.id.container
			Map.installMap(MapType.GoogleMap, "mymap", this, R.id.container);
		}
		catch(InstallMapException e) {
			Log.e("install_error", e.getMessage());
		}				
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		try {
			//incarcam harta cu numele mymap
			Map map = Map.loadMap(MapType.GoogleMap, "mymap");
			
			ArrayList<Location> points = new ArrayList<Location>();
			
			points.add(new Location(-33.866, 151.195));
			points.add(new Location(-18.142, 178.431));
			points.add(new Location(21.291, -157.821));
			map.drawPolyline(points);
			
			points.clear();
			points.add(new Location(-20.866, 151.195));
			points.add(new Location(-20.142, 190.431));
			points.add(new Location(30.291, -140.821));
			map.drawPolygon(points);
			
			Location loc = new Location(10, 160);
			map.drawPoint(loc);
			map.setZoomLevel(3);
			map.zoom(loc);
						
			Toast.makeText(getApplicationContext(), "Map Loading Successfull", 1).show();
		}
		catch(LoadMapException e) {
			Toast.makeText(getApplicationContext(), "Map Loading Failed", 1).show();
		}
		catch(DrawException e) {
			Toast.makeText(getApplicationContext(), "Cannot draw on map", 1).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {		
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
